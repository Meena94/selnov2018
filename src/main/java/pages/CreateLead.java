package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	public CreateLead()
	{
		PageFactory.initElements(driver,this);

	}
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement elecmp;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement elefirstname;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement elelastname;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement elesubmit;
@And("Enter the company name as(.*)")
	public CreateLead entercmpnam(String data) {
		type(elecmp,data);
		return this;
		
	}
@And("Enter the First Name as(.*)")
	public CreateLead frstnam(String data) {
		type(elefirstname,data);
		return this;
		
	}
@And("Enter the Last Name as(.*)")
	public CreateLead lastname(String data) {
		type(elelastname,data);
		return this;
		
	}
@And("Click CreateLead button")
	public ViewLead clicksubmit() {
		click(elesubmit);
		return new ViewLead();
		
	}

}
