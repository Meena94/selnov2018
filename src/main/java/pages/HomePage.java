package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	public HomePage()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA") WebElement elecrmsfa;
	@And("click on CRMSFA Link")
	public MyHomePage clickcrmsfa()
	{
		click(elecrmsfa);
		return new MyHomePage();
		
	}
}