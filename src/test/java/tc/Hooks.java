package tc;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
	
	@Before
	public void before(Scenario sc) {
		System.out.println(sc.getName());
		System.out.println(sc.getId());
		startResult();
		suiteTest = extent.createTest("createlead", "create a new lead");
		test = 	suiteTest.createNode("Leads");
		test.assignCategory("smoke");
		test.assignAuthor("meena");
		startApp("chrome", "http://leaftaps.com/opentaps");		
		

		
	}
	@After
	public void after(Scenario sc) {
		System.out.println(sc.getStatus());
		System.out.println(sc.isFailed());
		extent.flush();
		
	}
	

}
