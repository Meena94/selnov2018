//package tc;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class CL {
//	public ChromeDriver driver;
//	@Given("Open the Browser")
//	public void openTheBrowser() {
//	    // Write code here that turns the phrase above into concrete actions
//		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//		driver = new ChromeDriver();
//	}
//
//	@Given("Max the Browser")
//	public void maxTheBrowser() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.manage().window().maximize();
//
//	}
//
//	@Given("Set the Timeout")
//	public void setTheTimeout() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//	}
//
//	@Given("Launch the URL")
//	public void launchTheURL() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.get("http://leaftaps.com/opentaps/control/main");
//
//	}
//
//	@Given("Enter the username as (.*)")
//	public void enterTheUsernameAsDemoSalesManager(String data) {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementById("username").sendKeys(data);
//		
//	}
//
//	@Given("Enter the Password as (.*)")
//	public void enterThePasswordAsCrmsfa(String data) {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementById("password").sendKeys(data);
//
//	}
//
//	@When("Click on the Login Button")
//	public void clickOnTheLoginButton() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementByClassName("decorativeSubmit").click();
//	}
//
//	@Then("Verify the Login")
//	public void verifyTheLogin() {
//	    // Write code here that turns the phrase above into concrete actions
//		System.out.println("Login is Successful");
//		
//	}
//
//	@Then("click on CRM\\/SFA Link")
//	public void clickOnCRMSFALink() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementByLinkText("CRM/SFA").click();
//	}
//
//	@Then("Click Leads")
//	public void clickLeads() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementByLinkText("Leads").click();
//	}
//
//	@Then("Click Createleads")
//	public void clickCreateleads() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementByLinkText("Create Lead").click();
//
//		
//	}
//
//	@Then("Enter the company name as (.*)")
//	public void enterTheCompanyNameAsRattletech(String data) {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementById("createLeadForm_companyName").sendKeys(data);
//
//	}
//
//	@Then("Enter the First Name as (.*)")
//	public void enterTheFirstNameAsMeena(String data) {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementById("createLeadForm_firstName").sendKeys(data);
//
//	    
//	}
//
//	@Then("Enter the Last Name as (.*)")
//	public void enterTheLastNameAsVani(String data) {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementById("createLeadForm_lastName").sendKeys(data);
//
//	    
//	}
//
//	@Then("Click CreateLead button")
//	public void clickCreateLeadButton() {
//	    // Write code here that turns the phrase above into concrete actions
//		driver.findElementByClassName("smallSubmit").click();
//	    
//	}
//
//	@Then("Verify the First Name Field")
//	public void verifyTheFirstNameField() {
//	    // Write code here that turns the phrase above into concrete actions
//		WebElement ele = driver.findElementById("viewLead_firstName_sp");
//		String text=ele.getText();
//		if (text.contains("Meena")) 
//			
//		{
//		 System.out.println("The entered text is matching");
//		 
//}
//		else
//		{
//			System.out.println("The value is not matched");
//		}
//		
//		
//	    
//	}
//
//
//
//}
